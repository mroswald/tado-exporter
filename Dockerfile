FROM --platform=$BUILDPLATFORM rust:latest AS builder

ENV TARGET=armv7-unknown-linux-musleabihf 
ENV CARGO_TARGET_ARMV7_UNKNOWN_LINUX_MUSLEABIHF_LINKER=arm-linux-gnueabihf-gcc
ENV CC=arm-linux-gnueabihf-gcc

WORKDIR /usr/src/tado-exporter

RUN apt-get update && \
    apt-get -y install ca-certificates libssl-dev musl-tools musl musl-dev \
    build-essential gcc-arm-linux-gnueabihf && \
    rm -rf /var/lib/apt/lists/*

COPY . .

RUN rustup target add ${TARGET}
RUN cargo update
RUN cargo build --target ${TARGET} --release
RUN file /usr/src/tado-exporter/target/armv7-unknown-linux-musleabihf/release/tado-exporter

# issue with alpine >=3.13 and docker
# https://gitlab.alpinelinux.org/alpine/aports/-/issues/12091
FROM alpine:3.12
LABEL name="tado-exporter"

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/src/tado-exporter/target/armv7-unknown-linux-musleabihf/release/tado-exporter /

CMD ["/tado-exporter"]